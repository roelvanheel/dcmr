package org.example.beans;

import java.util.Calendar;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.example.beans.Imageset;
import org.example.beans.Geolocation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.onehippo.cms7.essentials.components.rest.adapters.HippoHtmlAdapter;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;

@XmlRootElement(name = "newsdocument")
@XmlAccessorType(XmlAccessType.NONE)
@HippoEssentialsGenerated(internalName = "dcmr:newsdocument")
@Node(jcrType = "dcmr:newsdocument")
public class NewsDocument extends HippoDocument {
	/** 
	 * The document type of the news document.
	 */
	public final static String DOCUMENT_TYPE = "dcmr:newsdocument";
	private final static String TITLE = "dcmr:title";
	private final static String DATE = "dcmr:date";
	private final static String INTRODUCTION = "dcmr:introduction";
	private final static String IMAGE = "dcmr:image";
	private final static String CONTENT = "dcmr:content";
	private final static String LOCATION = "dcmr:location";
	private final static String AUTHOR = "dcmr:author";
	private final static String SOURCE = "dcmr:source";

	/** 
	 * Get the title of the document.
	 * @return the title
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:title")
	public String getTitle() {
		return getProperty(TITLE);
	}

	/** 
	 * Get the date of the document.
	 * @return the date
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:date")
	public Calendar getDate() {
		return getProperty(DATE);
	}

	/** 
	 * Get the introduction of the document.
	 * @return the introduction
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:introduction")
	public String getIntroduction() {
		return getProperty(INTRODUCTION);
	}

	/** 
	 * Get the main content of the document.
	 * @return the content
	 */
	@XmlJavaTypeAdapter(HippoHtmlAdapter.class)
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:content")
	public HippoHtml getContent() {
		return getHippoHtml(CONTENT);
	}

	/** 
	 * Get the location of the document.
	 * @return the location
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:location")
	public String getLocation() {
		return getProperty(LOCATION);
	}

	/** 
	 * Get the author of the document.
	 * @return the author
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:author")
	public String getAuthor() {
		return getProperty(AUTHOR);
	}

	/** 
	 * Get the source of the document.
	 * @return the source
	 */
	@XmlElement
	@HippoEssentialsGenerated(internalName = "dcmr:source")
	public String getSource() {
		return getProperty(SOURCE);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:image")
	public Imageset getImage() {
		return getLinkedBean("dcmr:image", Imageset.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:relatednews")
	public List<HippoBean> getRelatednews() {
		return getLinkedBeans("dcmr:relatednews", HippoBean.class);
	}
}