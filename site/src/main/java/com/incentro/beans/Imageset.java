package com.incentro.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageBean;

@HippoEssentialsGenerated(internalName = "dcmr:imageset")
@Node(jcrType = "dcmr:imageset")
public class Imageset extends HippoGalleryImageSet {
	@HippoEssentialsGenerated(internalName = "dcmr:small")
	public HippoGalleryImageBean getSmall() {
		return getBean("dcmr:small", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:large")
	public HippoGalleryImageBean getLarge() {
		return getBean("dcmr:large", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:smallsquare")
	public HippoGalleryImageBean getSmallsquare() {
		return getBean("dcmr:smallsquare", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:mediumsquare")
	public HippoGalleryImageBean getMediumsquare() {
		return getBean("dcmr:mediumsquare", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:largesquare")
	public HippoGalleryImageBean getLargesquare() {
		return getBean("dcmr:largesquare", HippoGalleryImageBean.class);
	}

	@HippoEssentialsGenerated(internalName = "dcmr:banner")
	public HippoGalleryImageBean getBanner() {
		return getBean("dcmr:banner", HippoGalleryImageBean.class);
	}
}
