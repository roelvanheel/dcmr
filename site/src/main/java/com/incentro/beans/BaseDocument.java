package com.incentro.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="dcmr:basedocument")
public class BaseDocument extends HippoDocument {

}
