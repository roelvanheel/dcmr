<#include "../include/imports.ftl">

<@hst.setBundle basename="essentials.global"/>
<@hst.include ref="container"/>


<div class="container">
  <sub><@fmt.message key="footer.text" var="footer"/>${footer?html}</sub>
</div>
