<!doctype html>
<#include "../include/imports.ftl">

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="<@hst.webfile  path="/css/bootstrap.css"/>" type="text/css"/>
	<link rel="stylesheet" href="<@hst.webfile  path="/css/dcmr.css"/>" type="text/css"/>    
    <@hst.defineObjects/>
    <#if hstRequest.requestContext.cmsRequest>
      <link rel="stylesheet" href="<@hst.webfile  path="/css/cms-request.css"/>" type="text/css"/>
    </#if>
<@hst.headContributions categoryExcludes="htmlBodyEnd, scripts" xhtml=true/>
</head>
<body>
<div id="wrapper" class="container">
    <div id="header" class="row">
    	<div class="top-bar"></div>
        <div class="col-md-6 col-md-offset-3">        
        	<div id="top-menu">
        		<@hst.include ref="top"/>
        	</div>
        	<div id="main-menu">
        		<@hst.include ref="menu"/>
        	</div>        	
        </div>
        <div class="col-md-3">
			<div class="searchbox">
  				<form action="<@hst.link siteMapItemRefId="search" />" method="get">
    				<input type="text" class="searchbox-inputtext" id="searchbox-inputtext" name="query" placeholder="Search" /> 
    				<label class="searchbox-icon" for="searchbox-inputtext"></label> 
  				</form>
			</div>        
        </div>
    </div>
    <div id="visuals" class="row">
        <@hst.include ref="visuals"/>
    </div>
    <div id="content" class="row">
        <@hst.include ref="main"/>
    </div>    
</div>
<div id="footer">
        <@hst.include ref="footer"/>
    </div>
    
<@hst.headContributions categoryIncludes="htmlBodyEnd, scripts" xhtml=true/>
</body>
</html>